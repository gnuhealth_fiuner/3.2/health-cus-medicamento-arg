# -*- coding: utf-8 -*-
# This file is part of health_cus_medicamento_arg module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import sys
from dateutil.relativedelta import relativedelta
from datetime import datetime

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, Or
from trytond.transaction import Transaction

__all__ = ['PatientPrescriptionOrder', 'PrescriptionLine',
    'DeliverPrescriptionLineStart', 'DeliverPrescriptionLine']

MOD_Z_PATIENT = ('trytond.modules.z_patient' in sys.modules)


class PatientPrescriptionOrder(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.order'

    chronic_patient = fields.Boolean('Chronic patient',
        states={
            'readonly': Or(
                Eval('state') != 'draft',
                Bool(Eval('prescription_line'))),
            },
        depends=['state', 'prescription_line'])
    delivery_duration = fields.Integer('Delivery duration',
        states={
            'readonly': Or(
                Eval('state') != 'draft',
                Bool(Eval('prescription_line'))),
            'required': Bool(Eval('chronic_patient')),
            },
        depends=['state', 'chronic_patient', 'prescription_line'],
        help='Period during which the medication should be delivered '
        'to the patient (in months)')
    delivery_pending = fields.Function(fields.Boolean('Pending deliveries'),
        'get_delivery_pending')
    icon = fields.Function(fields.Char('Icon'), 'get_icon')

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        if not cls.prescription_line.context:
            cls.prescription_line.context = {}
        cls.prescription_line.context.update({
            'delivery_duration': Eval('delivery_duration'),
            })
        if 'delivery_duration' not in cls.prescription_line.depends:
            cls.prescription_line.depends.append('delivery_duration')
        cls._error_messages.update({
            'delete_draft': ('You can not delete prescription "%s" because '
                'it is not in draft state'),
            })

    @staticmethod
    def default_chronic_patient():
        return False

    @fields.depends('patient')
    def on_change_patient(self):
        super(PatientPrescriptionOrder, self).on_change_patient()
        if MOD_Z_PATIENT and self.patient and self.patient.cronico == 'Si':
            self.chronic_patient = True
            self.on_change_chronic_patient()

    @fields.depends('chronic_patient')
    def on_change_chronic_patient(self):
        if self.chronic_patient:
            self.delivery_duration = 3
        else:
            self.delivery_duration = None

    def get_delivery_pending(self, name=None):
        for line in self.prescription_line:
            if line.deliveries < (line.duration or 1):
                return True
        return False

    def get_icon(self, name=None):
        if self.delivery_pending:
            return 'color-yellow'
        return None

    @classmethod
    def delete(cls, prescriptions):
        for prescription in prescriptions:
            if prescription.state != 'draft':
                cls.raise_user_error('delete_draft', (prescription.rec_name,))
        super(PatientPrescriptionOrder, cls).delete(prescriptions)


class PrescriptionLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.line'

    prescription_state = fields.Function(fields.Char('State'),
        'get_prescription_state')
    deliveries = fields.Integer('Deliveries')
    deliveries_string = fields.Function(fields.Char('Deliveries'),
        'get_deliveries_string')
    delivery_available = fields.Function(fields.Boolean('Pending deliveries'),
        'get_delivery_available')
    total_quantity = fields.Function(fields.Integer('Total quantity'),
        'on_change_with_total_quantity')

    @classmethod
    def __setup__(cls):
        super(PrescriptionLine, cls).__setup__()
        cls.medicament.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.medicament.depends:
            cls.medicament.depends.append('prescription_state')
        cls.quantity.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.quantity.depends:
            cls.quantity.depends.append('prescription_state')
        cls.indication.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.indication.depends:
            cls.indication.depends.append('prescription_state')

        cls.duration.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.duration.depends:
            cls.duration.depends.append('prescription_state')
        cls.duration_period.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.duration_period.depends:
            cls.duration_period.depends.append('prescription_state')
        cls.start_treatment.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.start_treatment.depends:
            cls.start_treatment.depends.append('prescription_state')
        cls.end_treatment.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.end_treatment.depends:
            cls.end_treatment.depends.append('prescription_state')
        cls._buttons.update({
            'deliver': {
                'invisible': ~Eval('delivery_available', False),
                },
            })

    @staticmethod
    def default_deliveries():
        return 0

    @staticmethod
    def default_duration():
        return Transaction().context.get('delivery_duration', None)

    @staticmethod
    def default_duration_period():
        if Transaction().context.get('delivery_duration', False):
            return 'months'
        return 'days'

    @staticmethod
    def default_end_treatment():
        duration = Transaction().context.get('delivery_duration', False)
        if duration:
            now = datetime.now()
            return now + relativedelta(months=+duration)

    def get_prescription_state(self, name=None):
        return self.name.state

    def get_deliveries_string(self, name=None):
        return '%s / %s' % (self.deliveries, self.duration or 1)

    def get_delivery_available(self, name=None):
        if (self.name.state == 'done' and
                self.name.pharmacy and
                self.deliveries < (self.duration or 1)):
            return True
        return False

    @fields.depends('quantity', 'duration')
    def on_change_with_total_quantity(self, name=None):
        return self.quantity * (self.duration or 1)

    @classmethod
    @ModelView.button_action(
        'health_cus_medicamento_arg.wizard_deliver_prescription_line')
    def deliver(cls, lines):
        pass


class DeliverPrescriptionLineStart(ModelView):
    'Deliver Prescription Line'
    __name__ = 'gnuhealth.prescription.line.deliver.start'

    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        readonly=True)
    product = fields.Many2One('product.product', 'Product')
    lot_required = fields.Boolean('Lot required')
    lot = fields.Many2One('stock.lot', 'Lot',
        domain=[('product', '=', Eval('product'))],
        states={'required': Bool(Eval('lot_required'))},
        context={'locations': [Eval('from_location')]},
        depends=['product', 'lot_required', 'from_location'])
    quantity = fields.Integer('Quantity', readonly=True)
    prescription = fields.Many2One('gnuhealth.prescription.order',
        'Prescription', )
    from_location = fields.Many2One('stock.location', 'From Location')
    to_location = fields.Many2One('stock.location', 'To Location')


class DeliverPrescriptionLine(Wizard):
    'Deliver Prescription Line'
    __name__ = 'gnuhealth.prescription.line.deliver'

    start_state = 'check'
    check = StateTransition()
    start = StateView('gnuhealth.prescription.line.deliver.start',
        'health_cus_medicamento_arg.view_deliver_prescription_line_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'confirm', 'tryton-ok', True),
            ])
    confirm = StateTransition()

    def transition_check(self):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        line = PrescriptionLine(Transaction().context['active_id'])
        if not line.delivery_available:
            return 'end'
        return 'start'

    def default_start(self, fields):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        line = PrescriptionLine(Transaction().context['active_id'])
        prescription = line.name
        from_location = prescription.pharmacy.warehouse
        if from_location.type == 'warehouse':
            from_location = from_location.storage_location
        to_location = prescription.patient.name.customer_location
        product = line.medicament.name
        lot_required = product.lot_is_required(from_location, to_location)

        return {
            'medicament': line.medicament.id,
            'product': product.id,
            'lot_required': lot_required,
            'quantity': line.quantity,
            'prescription': prescription.id,
            'from_location': from_location.id,
            'to_location': to_location.id,
            }

    def transition_confirm(self):
        pool = Pool()
        StockMove = pool.get('stock.move')
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        # Create Stock Move
        move = StockMove()
        move.origin = self.start.prescription
        move.planned_date = self.start.prescription.prescription_date.date()
        move.effective_date = datetime.today().date()
        move.from_location = self.start.from_location
        move.to_location = self.start.to_location
        move.product = self.start.product
        move.lot = self.start.lot
        move.quantity = self.start.quantity
        move.uom = self.start.product.default_uom
        move.unit_price = self.start.product.list_price
        StockMove.save([move])
        StockMove.do([move])

        # Update deliveries in Prescription line
        line = PrescriptionLine(Transaction().context['active_id'])
        line.deliveries += 1
        line.save()

        return 'end'
