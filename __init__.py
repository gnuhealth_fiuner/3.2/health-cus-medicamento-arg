# -*- coding: utf-8 -*-
# This file is part of z_health_stock_dangelo module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import health_stock


def register():
    Pool.register(
        health_stock.PatientPrescriptionOrder,
        health_stock.PrescriptionLine,
        health_stock.DeliverPrescriptionLineStart,
        module='health_cus_medicamento_arg', type_='model')
    Pool.register(
        health_stock.DeliverPrescriptionLine,
        module='health_cus_medicamento_arg', type_='wizard')
