# This file is part of z_health_stock_dangelo module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class TestCase(ModuleTestCase):
    'Test module'
    module = 'z_health_stock_dangelo'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TestCase))
    return suite
